#include <string.h>
#include <gtk/gtk.h>
#include "firm_search.h"
#include "firm_login.h"
#include "../requests.h"
#include "../database.h"

int first_time = 1;

GtkListStore *gpuList;
GtkListStore *cpuList;
GtkListStore *ramList;
GtkListStore *romList;

static void do_req(char *r);

typedef struct s_data
{
	char *id;
	char *pcid;
	char *type;
} t_data;

static void
change (GtkComboBox *comb, t_data *data)
{
	GtkTreeIter iter;
	char *newid;
	gtk_combo_box_get_active_iter (comb, &iter);
	GtkTreeModel *model = gtk_combo_box_get_model(comb);
	gtk_tree_model_get (model, &iter, COLUMN_STRING_ID, &newid, -1);
	char *req = g_strjoin("", "UPDATE Computer SET ", data->type, "=", newid, " WHERE ID=", data->pcid, NULL);
	sql_run(g_sqlite, req, NULL, NULL);
}

static void
changeEntry (GtkEntry *ent, t_data *data)
{
	const char *chr = gtk_entry_get_text(ent);
	char *req = g_strjoin("", "UPDATE Computer SET ", data->type, "='", chr, "' WHERE ID=", data->pcid, NULL);
	sql_run(g_sqlite, req, NULL, NULL);
}

static int
get_n_from_model(GtkTreeModel *model, char *id)
{
	GtkTreeIter iter;
	char *tmpid;
	int n = 0;

	gtk_tree_model_get_iter_first (model, &iter);
	do {
		gtk_tree_model_get(model, &iter, COLUMN_STRING_ID, &tmpid, -1);
		if (!strcmp(id, tmpid))
			return (n);
		++n;
	}
	while (gtk_tree_model_iter_next(model, &iter));
	return (n);
}

static void
add_combo(GtkWidget *to, GtkListStore *store, char *id, char *pcid, char *type)
{
	GtkCellRenderer *cell;
	t_data *d = malloc(sizeof(t_data));
	d->type = g_strdup(type);
	d->id   = g_strdup(id);
	d->pcid = g_strdup(pcid);

	GtkWidget *comb = gtk_combo_box_new_with_model((GtkTreeModel *)store);
	gtk_box_pack_end(GTK_BOX(to), comb, 0, 0, 2);
	cell = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (comb), cell, TRUE);
	gtk_cell_layout_add_attribute(GTK_CELL_LAYOUT(comb), cell, "text", COLUMN_STRING_NAME);
	int n = get_n_from_model((GtkTreeModel *)store, id);
	gtk_combo_box_set_active (GTK_COMBO_BOX(comb), n);
	g_signal_connect(comb, "changed", G_CALLBACK(change), (gpointer)d);
}

static void
deletePc (GtkButton *button, t_data *data)
{
	char *req = g_strjoin("", "DELETE FROM Computer WHERE ID=", data->pcid, NULL);
	sql_run(g_sqlite, req, NULL, NULL);
	free(req);
	do_req(NULL);
}

static int
append_list(void *data, int argc, char **argv, char **azColName)
{
	GtkCellRenderer *cell;
	int n;
	char *l = g_strjoin(" ", argv[1], argv[2], NULL);
	GtkWidget *child = gtk_expander_new(l);
	GtkWidget *inputLine  = gtk_entry_new();
	GtkWidget *inputModel = gtk_entry_new();
	GtkWidget *inputPrice = gtk_entry_new();
	GtkWidget *deleteButton = gtk_button_new_with_label("Delete");

	gtk_entry_set_text(GTK_ENTRY(inputLine), argv[1]);
	gtk_entry_set_text(GTK_ENTRY(inputModel), argv[2]);
	gtk_entry_set_text(GTK_ENTRY(inputPrice), argv[8]);
	GtkWidget *hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	GtkWidget *vboxlist = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	GtkWidget *vboxbutt = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_container_add(GTK_CONTAINER(child), hbox);
	gtk_box_pack_start(GTK_BOX(hbox), vboxlist, 0, 0, 0);
	gtk_box_pack_end(GTK_BOX(hbox), vboxbutt, 0, 0, 0);

	gtk_box_pack_end(GTK_BOX(vboxlist), deleteButton, 0, 0, 2);
	gtk_box_pack_end(GTK_BOX(vboxlist), inputPrice, 0, 0, 2);
	add_combo(vboxlist, romList, argv[6], argv[0], "DISK");
	add_combo(vboxlist, ramList, argv[5], argv[0], "RAM");
	add_combo(vboxlist, gpuList, argv[4], argv[0], "GPU");
	add_combo(vboxlist, cpuList, argv[3], argv[0], "CPU");
	gtk_box_pack_end(GTK_BOX(vboxlist), inputModel, 0, 0, 2);
	gtk_box_pack_end(GTK_BOX(vboxlist), inputLine, 0, 0, 2);

	t_data *dl = malloc(sizeof(t_data));
	t_data *dm = malloc(sizeof(t_data));
	t_data *dp = malloc(sizeof(t_data));
	dm->pcid = dp->pcid = dl->pcid = g_strdup(argv[0]);
	dl->type = g_strdup("Line");
	dm->type = g_strdup("Model");
	dp->type = g_strdup("Price");
	g_signal_connect(deleteButton, "clicked", G_CALLBACK(deletePc), (gpointer)dl);
	g_signal_connect(inputLine,  "changed", G_CALLBACK(changeEntry), (gpointer)dl);
	g_signal_connect(inputModel, "changed", G_CALLBACK(changeEntry), (gpointer)dm);
	g_signal_connect(inputPrice, "changed", G_CALLBACK(changeEntry), (gpointer)dp);

	gtk_list_box_insert(GTK_LIST_BOX(data), child, -1);
	gtk_widget_show_all(data);
	return 0;
}

gchar*
req(gchar *str)
{
	char *r;
	return g_strjoin("",
		"SELECT "
		"pc.ID,"
		"pc.Line, "
		"pc.Model, "
		"cpu.ID as CPU, "
		"gpu.ID as GPU, "
		"ram.ID as RAM, "
		"dis.ID as DISK, "
		"pc.Rating, "
		"pc.Price "
		"FROM Computer pc "
		"LEFT JOIN Firms fm       ON pc.FirmID = fm.ID "
		"LEFT JOIN Components cpu ON pc.CPU    = cpu.ID "
		"LEFT JOIN Components gpu ON pc.GPU    = gpu.ID "
		"LEFT JOIN Components ram ON pc.RAM    = ram.ID "
		"LEFT JOIN Components dis ON pc.DISK   = dis.ID "
		"WHERE "
		"pc.FirmID=", firmID, " AND "
		"(pc.Line  LIKE '\%", str, "\%' OR ",
		"pc.Model LIKE '\%", str, "\%');",
		NULL);
}

static void
do_req(char *r)
{
	if (!r)
		r = req("");
	GList *rows = gtk_container_get_children(GTK_CONTAINER(list2));
	while (rows != NULL)
	{
		gtk_container_remove(GTK_CONTAINER(list2), rows->data);
		rows = rows->next;
	}
	sql_run(g_sqlite, r, list2, &append_list);
	free(r);
}

void
firm_pc_search(GtkEditable *editable, gpointer data)
{
	gchar *str = gtk_editable_get_chars(editable, 0, -1);
	char *r = req(str);
	do_req(r);
}

static int
list_append(void *data, int argc, char **argv, char **azColName)
{
	GtkTreeIter iter;

	gtk_list_store_append((GtkListStore *)data, &iter);
	gtk_list_store_set((GtkListStore *)data, &iter,
						COLUMN_STRING_ID,   argv[0],
						COLUMN_STRING_NAME, argv[2],
						-1);
	return 0;
}

void
firm_search_init()
{
	do_req(NULL);
	if (first_time)
	{
		cpuList = gtk_list_store_new (N_COLUMNS, G_TYPE_STRING, G_TYPE_STRING);
		sql_run(g_sqlite, "SELECT * FROM Components WHERE Type LIKE \'CPU\'", cpuList, &list_append);
		gpuList = gtk_list_store_new (N_COLUMNS, G_TYPE_STRING, G_TYPE_STRING);
		sql_run(g_sqlite, "SELECT * FROM Components WHERE Type LIKE \'GPU\'", gpuList, &list_append);
		ramList = gtk_list_store_new (N_COLUMNS, G_TYPE_STRING, G_TYPE_STRING);
		sql_run(g_sqlite, "SELECT * FROM Components WHERE Type LIKE \'RAM\'", ramList, &list_append);
		romList = gtk_list_store_new (N_COLUMNS, G_TYPE_STRING, G_TYPE_STRING);
		sql_run(g_sqlite, "SELECT * FROM Components WHERE Type LIKE \'DISK\'", romList, &list_append);
		first_time = 0;
	}
}
