#include <string.h>
#include <gtk/gtk.h>
#include "firm_search.h"
#include "firm_login.h"
#include "../requests.h"
#include "../database.h"

GtkListStore *pcGpuList;
GtkListStore *pcCpuList;
GtkListStore *pcRamList;
GtkListStore *pcRomList;

void
pc_add_button_clicked (GtkButton *button, gpointer data)
{

	if (firmID == NULL)
		return;

	GtkTreeIter iter;
	GtkTreeModel *tree_model;

	const char *cpu_id;
	tree_model = gtk_combo_box_get_model(GTK_COMBO_BOX(pc_add_cpu));
	gtk_combo_box_get_active_iter(GTK_COMBO_BOX(pc_add_cpu), &iter);
	gtk_tree_model_get(tree_model, &iter, COLUMN_STRING_ID, &cpu_id, -1);

	const char *gpu_id;
	tree_model = gtk_combo_box_get_model(GTK_COMBO_BOX(pc_add_gpu));
	gtk_combo_box_get_active_iter(GTK_COMBO_BOX(pc_add_gpu), &iter);
	gtk_tree_model_get(tree_model, &iter, COLUMN_STRING_ID, &gpu_id, -1);

	const char *ram_id;
	tree_model = gtk_combo_box_get_model(GTK_COMBO_BOX(pc_add_ram));
	gtk_combo_box_get_active_iter(GTK_COMBO_BOX(pc_add_ram), &iter);
	gtk_tree_model_get(tree_model, &iter, COLUMN_STRING_ID, &ram_id, -1);

	const char *rom_id;
	tree_model = gtk_combo_box_get_model(GTK_COMBO_BOX(pc_add_rom));
	gtk_combo_box_get_active_iter(GTK_COMBO_BOX(pc_add_rom), &iter);
	gtk_tree_model_get(tree_model, &iter, COLUMN_STRING_ID, &rom_id, -1);

	const char *line  = gtk_entry_get_text(GTK_ENTRY(pc_add_line));
	const char *model = gtk_entry_get_text(GTK_ENTRY(pc_add_model));
	const char *price = gtk_entry_get_text(GTK_ENTRY(pc_add_price));

	char *req = g_strjoin("", "INSERT INTO Computer('FirmID', 'Line', 'Model', 'Rating', 'CPU', 'GPU', 'RAM', 'DISK', Price)"
			"VALUES(", firmID, ", '", line, "', '", model, "', ", "0, ", cpu_id, ", ", gpu_id, ", ", ram_id, ", ", rom_id, ", ", price, ")", NULL);
	sql_run(g_sqlite, req, NULL, NULL);
	free(req);
}

static int
list_append(void *data, int argc, char **argv, char **azColName)
{
	GtkTreeIter iter;

	gtk_list_store_append((GtkListStore *)data, &iter);
	gtk_list_store_set((GtkListStore *)data, &iter,
						COLUMN_STRING_ID,   argv[0],
						COLUMN_STRING_NAME, argv[2],
						-1);
	return 0;
}

void
pc_add_init()
{
	pcCpuList = gtk_list_store_new (N_COLUMNS, G_TYPE_STRING, G_TYPE_STRING);
	sql_run(g_sqlite, "SELECT * FROM Components WHERE Type LIKE \'CPU\'", pcCpuList, &list_append);
	gtk_combo_box_set_model(GTK_COMBO_BOX(pc_add_cpu), GTK_TREE_MODEL(pcCpuList));
	pcGpuList = gtk_list_store_new (N_COLUMNS, G_TYPE_STRING, G_TYPE_STRING);
	sql_run(g_sqlite, "SELECT * FROM Components WHERE Type LIKE \'GPU\'", pcGpuList, &list_append);
	gtk_combo_box_set_model(GTK_COMBO_BOX(pc_add_gpu), GTK_TREE_MODEL(pcGpuList));
	pcRamList = gtk_list_store_new (N_COLUMNS, G_TYPE_STRING, G_TYPE_STRING);
	sql_run(g_sqlite, "SELECT * FROM Components WHERE Type LIKE \'RAM\'", pcRamList, &list_append);
	gtk_combo_box_set_model(GTK_COMBO_BOX(pc_add_ram), GTK_TREE_MODEL(pcRamList));
	pcRomList = gtk_list_store_new (N_COLUMNS, G_TYPE_STRING, G_TYPE_STRING);
	sql_run(g_sqlite, "SELECT * FROM Components WHERE Type LIKE \'DISK\'", pcRomList, &list_append);
	gtk_combo_box_set_model(GTK_COMBO_BOX(pc_add_rom), GTK_TREE_MODEL(pcRomList));

	GtkCellRenderer *cell;

	cell = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (pc_add_cpu), cell, TRUE);
	gtk_cell_layout_add_attribute(GTK_CELL_LAYOUT(pc_add_cpu), cell, "text", 1);
	gtk_combo_box_set_active (GTK_COMBO_BOX(pc_add_cpu), 0);
	gtk_widget_show_all(pc_add_cpu);

	cell = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (pc_add_gpu), cell, TRUE);
	gtk_cell_layout_add_attribute(GTK_CELL_LAYOUT(pc_add_gpu), cell, "text", 1);
	gtk_combo_box_set_active (GTK_COMBO_BOX(pc_add_gpu), 0);
	gtk_widget_show_all(pc_add_gpu);

	cell = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (pc_add_ram), cell, TRUE);
	gtk_cell_layout_add_attribute(GTK_CELL_LAYOUT(pc_add_ram), cell, "text", 1);
	gtk_combo_box_set_active (GTK_COMBO_BOX(pc_add_ram), 0);
	gtk_widget_show_all(pc_add_ram);

	cell = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (pc_add_rom), cell, TRUE);
	gtk_cell_layout_add_attribute(GTK_CELL_LAYOUT(pc_add_rom), cell, "text", 1);
	gtk_combo_box_set_active (GTK_COMBO_BOX(pc_add_rom), 0);
	gtk_widget_show_all(pc_add_rom);
}
