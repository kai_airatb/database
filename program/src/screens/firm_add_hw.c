#include <string.h>
#include <gtk/gtk.h>
#include "firm_search.h"
#include "firm_login.h"
#include "../requests.h"
#include "../database.h"

GtkListStore *hwTypesList;

void
hw_add_button_clicked(GtkButton *btn, gpointer data)
{
	const gchar *type = gtk_combo_box_get_active_id(GTK_COMBO_BOX(hw_add_type));
	const gchar *name = gtk_entry_get_text(GTK_ENTRY(hw_add_name));
	const gchar *desc = gtk_entry_get_text(GTK_ENTRY(hw_add_description));
	const gchar *perf = gtk_entry_get_text(GTK_ENTRY(hw_add_perfomance));
	char *req = g_strjoin("", "INSERT INTO Components('Type', 'Name', 'Description', 'Performance') VALUES('", type, "', '", name, "', '", desc, "', ", perf, ")", NULL);
	sql_run(g_sqlite, req, NULL, NULL);
	free(req);
}

void
hw_add_init()
{
	hwTypesList = gtk_list_store_new (1, G_TYPE_STRING);
	GtkTreeIter iter;
	gtk_list_store_append((GtkListStore *)hwTypesList, &iter);
	gtk_list_store_set((GtkListStore *)hwTypesList, &iter, 0, "CPU",  -1);
	gtk_list_store_append((GtkListStore *)hwTypesList, &iter);
	gtk_list_store_set((GtkListStore *)hwTypesList, &iter, 0, "GPU",  -1);
	gtk_list_store_append((GtkListStore *)hwTypesList, &iter);
	gtk_list_store_set((GtkListStore *)hwTypesList, &iter, 0, "RAM",  -1);
	gtk_list_store_append((GtkListStore *)hwTypesList, &iter);
	gtk_list_store_set((GtkListStore *)hwTypesList, &iter, 0, "DISK", -1);
	gtk_combo_box_set_model(GTK_COMBO_BOX(hw_add_type), (GTK_TREE_MODEL(hwTypesList)));

	GtkCellRenderer *cell;
	cell = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (hw_add_type), cell, TRUE);
	gtk_cell_layout_add_attribute(GTK_CELL_LAYOUT(hw_add_type), cell, "text", 0);
	gtk_combo_box_set_active (GTK_COMBO_BOX(hw_add_type), 0);

	gtk_widget_show_all(hw_add_type);
}
