#include <stdio.h>
#include <gtk/gtk.h>
#include "firm_login.h"
#include "firm_search.h"
#include "../requests.h"
#include "../database.h"

gchar *firmID = NULL;

static int
change_lable(void *data, int argc, char **argv, char **azColName){
	GtkWidget *label = GTK_WIDGET(data);
	firmID = g_strdup(argv[0]);
	gtk_label_set_text(GTK_LABEL(label), g_strjoin(" ", "Logged in as", argv[1], NULL));
	gtk_widget_show_all(data);
	firm_search_init();
	return 0;
}

static gchar*
req(gchar *str)
{
	return g_strjoin("",
		"SELECT fm.ID, fm.Name FROM Firms fm WHERE fm.Name LIKE '\%", str, "\%' "
		"ORDER BY fm.Name;",
		NULL);
}

void
firm_login_search(GtkEditable *editable, gpointer data)
{
	gchar *str = gtk_editable_get_chars(editable, 0, -1);
	gchar *r = req(str);
	sql_run(g_sqlite, r, data, &change_lable);
	free(r);
}
