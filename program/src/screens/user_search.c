#include <gtk/gtk.h>
#include "user_search.h"
#include "user_diff.h"
#include "../requests.h"
#include "../database.h"

GList *toggled = NULL;
GList *difflist = NULL;
int sort[2];

void
star_toggle(GtkToggleButton *button, gpointer data)
{
	char *req;
	if (gtk_toggle_button_get_active(button))
	{
		req = g_strjoin("", "UPDATE Computer SET Rating = Rating + 1 WHERE ID=", (char *)data, NULL);
		toggled = g_list_append(toggled, (gpointer)button);
	}
	else
	{
		req = g_strjoin("", "UPDATE Computer SET Rating = Rating - 1 WHERE ID=", (char *)data, NULL);
		toggled = g_list_remove(toggled, (gpointer)button);
	}
	sql_run(g_sqlite, req, NULL, NULL);
}

void
diff_toggle(GtkToggleButton *button, gpointer data)
{
	char *req;
	if (gtk_toggle_button_get_active(button))
	{
		if (g_list_length(difflist) < 2)
			difflist = g_list_append(difflist, data);
	}
	else
		difflist = g_list_remove(difflist, data);
	user_diff_init();
}

static int
append_list1(void *data, int argc, char **argv, char **azColName){
	char *l = g_strjoin(" ", argv[1], argv[2], argv[3], NULL);
	GtkWidget *child = gtk_expander_new(l);
	GtkWidget *hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	GtkWidget *vboxlist = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	GtkWidget *vboxbutt = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	GtkWidget *vote = gtk_toggle_button_new_with_label("Vote");
	g_signal_connect (vote, "toggled", G_CALLBACK(star_toggle), (gpointer)g_strdup(argv[0]));
	GtkWidget *add = gtk_toggle_button_new_with_label("Diff");
	g_signal_connect (add, "toggled", G_CALLBACK(diff_toggle), (gpointer)g_strdup(argv[0]));
	gtk_container_add(GTK_CONTAINER(child), hbox);
	gtk_box_pack_start(GTK_BOX(hbox), vboxlist, 0, 0, 0);
	gtk_box_pack_end(GTK_BOX(hbox), vboxbutt, 0, 0, 0);
	gtk_box_pack_start(GTK_BOX(vboxbutt), vote, 1, 1, 2);
	gtk_box_pack_start(GTK_BOX(vboxbutt), add, 1, 1, 2);
	while (--argc != 3)
	{
		GtkWidget *tmplabel = gtk_label_new(g_strjoin(": ", azColName[argc], argv[argc], NULL));
		gtk_label_set_xalign(GTK_LABEL(tmplabel), 0);
		gtk_widget_set_margin_start(tmplabel, 25);
		gtk_box_pack_end(GTK_BOX(vboxlist), tmplabel, 0, 0, 2);
	}
	gtk_list_box_insert(GTK_LIST_BOX(data), child, -1);
	gtk_widget_show_all(data);
	return 0;
}

gchar*
z1_1(gchar *str)
{
	char *r;
	if (sort[0] == 0 && sort[1] == 0)
		r = "fm.Name ASC, pc.Line ASC, pc.Model ASC";
	if (sort[0] == 0 && sort[1] == 1)
		r = "fm.Name DESC, pc.Line DESC, pc.Model DESC";
	if (sort[0] == 1 && sort[1] == 0)
		r = "pc.Price ASC";
	if (sort[0] == 1 && sort[1] == 1)
		r = "pc.Price DESC";
	return g_strjoin("",
		"SELECT ",
		"pc.ID,"
		"fm.Name, ",
		"pc.Line, ",
		"pc.Model, ",
		"cpu.Name as CPU, ",
		"gpu.Name as GPU, ",
		"ram.Name as RAM, ",
		"dis.Name as DISK, ",
		"pc.Rating, ",
		"pc.Price ",
		"FROM Computer pc ",
		"LEFT JOIN Firms fm       ON pc.FirmID = fm.ID ",
		"LEFT JOIN Components cpu ON pc.CPU    = cpu.ID ",
		"LEFT JOIN Components gpu ON pc.GPU    = gpu.ID ",
		"LEFT JOIN Components ram ON pc.RAM    = ram.ID ",
		"LEFT JOIN Components dis ON pc.DISK   = dis.ID ",
		"WHERE ",
		"pc.Line  LIKE '\%", str, "\%' OR ",
		"pc.Model LIKE '\%", str, "\%' OR ",
		"fm.Name  LIKE '\%", str, "\%' "
		"ORDER BY ",
		r, ";",
		NULL);
}

static void
do_req(char *req)
{
	if (!req)
		req = z1_1("");
	GList *rows = gtk_container_get_children(GTK_CONTAINER(list1));
	while (rows != NULL)
	{
		gtk_container_remove(GTK_CONTAINER(list1), rows->data);
		rows = rows->next;
	}
	sql_run(g_sqlite, req, list1, &append_list1);
	free(req);
}

void
user_search_type_toggle(GtkToggleButton *button, gpointer data)
{
	if (gtk_toggle_button_get_active(button))
		sort[0] = 0;
	else
		sort[0] = 1;
	do_req(NULL);
}

void
user_search_direction_toggle(GtkToggleButton *button, gpointer data)
{
	if (gtk_toggle_button_get_active(button))
		sort[1] = 0;
	else
		sort[1] = 1;
	do_req(NULL);
}

void
search1_1(GtkEditable *editable, gpointer data)
{
	gchar *str = gtk_editable_get_chars(editable, 0, -1);
	char *req = z1_1(str);
	do_req(req);
}

void
user_search_init()
{
	sort[0] = 1;
	sort[1] = 1;
	do_req(NULL);
}
