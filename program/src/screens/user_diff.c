#include <stdio.h>
#include <string.h>
#include <sqlite3.h>
#include <gtk/gtk.h>
#include "user_diff.h"
#include "user_search.h"
#include "../requests.h"
#include "../database.h"


GList *per1 = NULL;
GList *per2 = NULL;

static void
print()
{
	int i = 0;
	GList *p1 = per1;
	GList *p2 = per2;

	while (p1 != NULL)
	{
		GtkWidget *label1 = gtk_label_new("");
		GtkWidget *label2 = gtk_label_new("");
		if (i > 3 && i < 8)
		{
			if (strcmp((char *)g_list_nth(p1, 6)->data, (char *)g_list_nth(p2, 6)->data) < 0)
			{
				gtk_label_set_markup(GTK_LABEL(label1), g_strjoin("", "<span foreground='green' weight='bold'>", (char*)p1->data, "</span>", NULL));
				gtk_label_set_markup(GTK_LABEL(label2), g_strjoin("", "<span foreground='red' weight='bold'>", (char*)p2->data, "</span>", NULL));
			}
			else if (strcmp((char *)g_list_nth(p1, 6)->data, (char *)g_list_nth(p2, 6)->data) > 0)
			{
				gtk_label_set_markup(GTK_LABEL(label1), g_strjoin("", "<span foreground='red' weight='bold'>", (char*)p1->data, "</span>", NULL));
				gtk_label_set_markup(GTK_LABEL(label2), g_strjoin("", "<span foreground='green' weight='bold'>", (char*)p2->data, "</span>", NULL));
			}
			else
			{
				gtk_label_set_label(GTK_LABEL(label1), (char *)p1->data);
				gtk_label_set_label(GTK_LABEL(label2), (char *)p2->data);
			}
		}
		else if (i == 8)
		{
			if (strcmp((char *)p1->data, (char *)p2->data) > 0)
			{
				gtk_label_set_markup(GTK_LABEL(label1), g_strjoin("", "<span foreground='green' weight='bold'>", (char*)p1->data, "</span>", NULL));
				gtk_label_set_markup(GTK_LABEL(label2), g_strjoin("", "<span foreground='red' weight='bold'>", (char*)p2->data, "</span>", NULL));
			}
			else if (strcmp((char *)p1->data, (char *)p2->data) < 0)
			{
				gtk_label_set_markup(GTK_LABEL(label1), g_strjoin("", "<span foreground='red' weight='bold'>", (char*)p1->data, "</span>", NULL));
				gtk_label_set_markup(GTK_LABEL(label2), g_strjoin("", "<span foreground='green' weight='bold'>", (char*)p2->data, "</span>", NULL));
			}
			else
			{
				gtk_label_set_label(GTK_LABEL(label1), (char *)p1->data);
				gtk_label_set_label(GTK_LABEL(label2), (char *)p2->data);
			}
		}
		else if (i == 9)
		{
			if (strcmp((char *)p1->data, (char *)p2->data) < 0)
			{
				gtk_label_set_markup(GTK_LABEL(label1), g_strjoin("", "<span foreground='green' weight='bold'>", (char*)p1->data, "</span>", NULL));
				gtk_label_set_markup(GTK_LABEL(label2), g_strjoin("", "<span foreground='red' weight='bold'>", (char*)p2->data, "</span>", NULL));
			}
			else if (strcmp((char *)p1->data, (char *)p2->data) > 0)
			{
				gtk_label_set_markup(GTK_LABEL(label1), g_strjoin("", "<span foreground='red' weight='bold'>", (char*)p1->data, "</span>", NULL));
				gtk_label_set_markup(GTK_LABEL(label2), g_strjoin("", "<span foreground='green' weight='bold'>", (char*)p2->data, "</span>", NULL));
			}
			else
			{
				gtk_label_set_label(GTK_LABEL(label1), (char *)p1->data);
				gtk_label_set_label(GTK_LABEL(label2), (char *)p2->data);
			}
		}
		else if (i <= 3)
		{
			gtk_label_set_label(GTK_LABEL(label1), (char *)p1->data);
			gtk_label_set_label(GTK_LABEL(label2), (char *)p2->data);
		}
		gtk_grid_attach(GTK_GRID(diff), label1, 0, i, 1, 1);
		gtk_grid_attach(GTK_GRID(diff), label2, 1, i, 1, 1);
		p1 = p1->next;
		p2 = p2->next;
		++i;
	}
	gtk_widget_show_all(diff);
}

static int
callback(void *data, int argc, char **argv, char **azColName)
{
	static int n = 0;
	int i = 0;
	while (i < argc)
	{
		if (n == 0)
			per1 = g_list_append(per1, (gpointer)g_strdup(argv[i++]));
		else if (n == 1)
			per2 = g_list_append(per2, (gpointer)g_strdup(argv[i++]));
	}
	if (n++)
		n = 0;
	return (0);
}

gchar*
get_req(gchar *id)
{
	return g_strjoin("",
		"SELECT ",
		"pc.ID,"
		"fm.Name, ",
		"pc.Line, ",
		"pc.Model, ",
		"cpu.Name as CPU, ",
		"gpu.Name as GPU, ",
		"ram.Name as RAM, ",
		"dis.Name as DISK, ",
		"pc.Rating, ",
		"pc.Price, ",
		"cpu.Performance as CPUPer, ",
		"gpu.Performance as GPUPer, ",
		"RAM.Performance as RAMPer, ",
		"dis.Performance as DISKPer ",
		"FROM Computer pc ",
		"LEFT JOIN Firms fm       ON pc.FirmID = fm.ID ",
		"LEFT JOIN Components cpu ON pc.CPU    = cpu.ID ",
		"LEFT JOIN Components gpu ON pc.GPU    = gpu.ID ",
		"LEFT JOIN Components ram ON pc.RAM    = ram.ID ",
		"LEFT JOIN Components dis ON pc.DISK   = dis.ID ",
		"WHERE pc.ID=",
		id, ";",
		NULL);
}

void
user_diff_init()
{
	if (g_list_length(difflist) == 2)
	{
		char *req = get_req(difflist->data);
		sql_run(g_sqlite, req, NULL, callback);
		free(req);
		req = get_req(difflist->next->data);
		sql_run(g_sqlite, req, NULL, callback);
		free(req);
		print();
	}
	else
	{
		GList *rows = gtk_container_get_children(GTK_CONTAINER(diff));
		while (rows != NULL)
		{
			gtk_container_remove(GTK_CONTAINER(diff), rows->data);
			rows = rows->next;
		}
		g_clear_list(&per1, free);
		g_clear_list(&per2, free);
	}
}
