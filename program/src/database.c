#include <stdio.h>
#include <string.h>
#include <sqlite3.h>
#include <gtk/gtk.h>
#include "requests.h"
#include "screens/user_search.h"
#include "database.h"

sqlite3 *g_sqlite;
GtkWidget *window;
GtkWidget *list1;
GtkWidget *list2;
GtkWidget *diff;
GtkBuilder *builder;

GtkWidget *hw_add_type;
GtkWidget *hw_add_name;
GtkWidget *hw_add_description;
GtkWidget *hw_add_perfomance;
GtkWidget *hw_add_button;
GtkWidget *pc_add_line;
GtkWidget *pc_add_model;
GtkWidget *pc_add_cpu;
GtkWidget *pc_add_gpu;
GtkWidget *pc_add_ram;
GtkWidget *pc_add_rom;
GtkWidget *pc_add_price;
GtkWidget *pc_add_button;

static GtkWidget*
create_window (void)
{
	GError* error = NULL;

	builder = gtk_builder_new();
	if (!gtk_builder_add_from_file (builder, "../glade/new.glade", &error))
	{
		g_critical("Не могу загрузить файл: %s", error->message);
		g_error_free(error);
	}

	gtk_builder_connect_signals(builder, NULL);

	window = GTK_WIDGET(gtk_builder_get_object (builder, "window"));
	diff = GTK_WIDGET(gtk_builder_get_object (builder, "user_diff"));
	list1 = GTK_WIDGET(gtk_builder_get_object (builder, "list1"));
	list2 = GTK_WIDGET(gtk_builder_get_object (builder, "list2"));

	hw_add_type        = GTK_WIDGET(gtk_builder_get_object (builder, "hw_add_type"));
	hw_add_name        = GTK_WIDGET(gtk_builder_get_object (builder, "hw_add_name"));
	hw_add_description = GTK_WIDGET(gtk_builder_get_object (builder, "hw_add_description"));
	hw_add_perfomance  = GTK_WIDGET(gtk_builder_get_object (builder, "hw_add_perfomance"));
	hw_add_button      = GTK_WIDGET(gtk_builder_get_object (builder, "hw_add_button"));
	pc_add_line        = GTK_WIDGET(gtk_builder_get_object (builder, "pc_add_line"));
	pc_add_model       = GTK_WIDGET(gtk_builder_get_object (builder, "pc_add_model"));
	pc_add_cpu         = GTK_WIDGET(gtk_builder_get_object (builder, "pc_add_cpu"));
	pc_add_gpu         = GTK_WIDGET(gtk_builder_get_object (builder, "pc_add_gpu"));
	pc_add_ram         = GTK_WIDGET(gtk_builder_get_object (builder, "pc_add_ram"));
	pc_add_rom         = GTK_WIDGET(gtk_builder_get_object (builder, "pc_add_rom"));
	pc_add_price       = GTK_WIDGET(gtk_builder_get_object (builder, "pc_add_price"));
	pc_add_button      = GTK_WIDGET(gtk_builder_get_object (builder, "pc_add_button"));

	if (!window)
		g_critical("Ошибка при получении виджета окна");
	g_object_unref(builder);

	return window;
}

int
main (int argc, char *argv[])
{
	GtkWidget *window;
	int rc;
	char *sql;

	gtk_init(&argc, &argv);
	rc = sqlite3_open("../db/sqlite.sqlite", &g_sqlite);
	if(rc) {
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(g_sqlite));
		return(0);
	}
	window = create_window();
	user_search_init();
	gtk_widget_show(window);

	hw_add_init();
	pc_add_init();
	gtk_main();
	sqlite3_close(g_sqlite);
	return 0;
}
