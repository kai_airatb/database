#ifndef REQUESTS_H
# define REQUESTS_H

#include <sqlite3.h>

int
sql_run(sqlite3 *db, char *command, void *data,
		int (*callback)(void *, int, char **, char **));

#endif
