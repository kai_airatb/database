#ifndef DATABASE_H
# define DATABASE_H

#include <sqlite3.h>
#include <gtk/gtk.h>

extern sqlite3 *g_sqlite;
extern GtkWidget *window;
extern GtkWidget *diff;
extern GtkWidget *list1;
extern GtkWidget *list2;
extern GtkBuilder *builder;

extern GtkWidget *hw_add_type;
extern GtkWidget *hw_add_name;
extern GtkWidget *hw_add_description;
extern GtkWidget *hw_add_perfomance;
extern GtkWidget *hw_add_button;
extern GtkWidget *pc_add_line;
extern GtkWidget *pc_add_model;
extern GtkWidget *pc_add_cpu;
extern GtkWidget *pc_add_gpu;
extern GtkWidget *pc_add_ram;
extern GtkWidget *pc_add_rom;
extern GtkWidget *pc_add_price;
extern GtkWidget *pc_add_button;

void hw_add_init();
void pc_add_init();

#endif
