#include <stdio.h>
#include <sqlite3.h>
#include "requests.h"

int
sql_run(sqlite3 *db, char *command, void *data,
		int (*callback)(void *, int, char **, char **))
{
	char *zErrMsg = 0;
	char *sql;
	int rc;

	rc = sqlite3_exec(db, command, callback, data, &zErrMsg);
	
	if( rc != SQLITE_OK ) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		return (-1);
	}
	return 0;
}
